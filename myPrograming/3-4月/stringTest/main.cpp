#include<iostream>
#include<string.h>

using namespace std;

int main() {
    string str = "hoge";
    cout << str << endl;
    str += "foo";
    cout << str << endl; // "hgegfoo"

    string str1 = "123";
    string str2 = "456";
    str1 += str2;
    cout << str1 << endl; // "123456"

    string str3(10, 'a'); // 第一引数は数，第二引数は文字列で1文字のみでシングルクォートで括る
    cout << str3 << endl; // "aaaaaaaaaa"
    string str4("abcde"); // 引数なしの場合。こちらはダブルクォートで括る
    cout << str4 << endl; // "abcde"

    const char *str5 = "abc123def";
    string str6(str5 + 3, str5 + 6); // 1文字目を0番目として3番目から6番目までを指定。 -を指定しての減算は不可
    cout << str6 << endl; // "123"

    string str7 = "123456";
    cout << str7[3] << endl; // "4"

    string str8 = "01234567";
    str8.erase(str8.begin() + 2, str8.begin() + 5); // eraseで削除。begin()は必須で第一引数の箇所から第二引数の1つ手前まで削除
    cout << str8 << endl; // "01567"

    string str9 = "abcdefg";
    str9.erase(str9.begin() + 3); // 引数が1つの場合。第一引数の箇所だけを削除する
    cout << str9 << endl;

    string str10 = "";
    string str11 = "hoge";
    cout << str10.empty() << endl; // emptyで文字列が空かどうか判定。空: 1, 空じゃない:0
    cout << str11.empty() << endl;

    string str12 = "hogefoobar";
    cout << str12.size() << endl; // sizeで文字列の長さを出力

    string str13 = "abcdefgghij";
    cout << str13.substr(3) << endl; // substrで文字列抽出。引数が1つの場合はn番目以降の文字列を抽出
    cout << str13.substr(4, 5) << endl; // 引数が2つの場合はn番目からm文字抽出

    // splitは存在しない

    string str14 = "0123789";
    cout << str14.insert(4, "456") << endl; // insertで挿入。n文字目に任意文字列を挿入

    string str15 = "abcdefabcdef";
    cout << str15.find("d") << endl; // findで文字列検索。該当する位置を表示
    cout << str15.find("2") << endl; // 見つからない場合は"18446744073709551615"。std::nposというもの。unsigned intの最大値。static const size_type npos = -1

    string str16 = "foobarhoge";
    cout << str16.replace(3, 3, "foo") << endl; // replaceで置換。第一引数のn番目の文字から第二引数のm文字の文字列を，第三引数の文字列で置換

    string str17 = "abcd";
    str17.push_back('e'); // push_backで末尾に文字列を追加。1文字のみ指定可能
    cout << str17 << endl; // "abcde"
    str17.pop_back(); // pop_backで末尾の文字を削除。引数は指定不可
    cout << str17 << endl; // "abcd"
}