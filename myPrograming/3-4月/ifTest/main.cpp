#include <iostream>

using namespace std;

int inputNum();

int main() {
    int x;
    x = inputNum();
    if(x >= 10) {
        cout << "入力された数字は10以上です。x: " << x << endl;
    } else if(x >= 5 && x < 10) {
        cout << "入力された数字は5以上10未満です。x: " << x << endl;
    } else {
        cout << "入力された数字は5未満です。x: " << x << endl;
    }
}

int inputNum() {
    int x;
    cout << "xの数字を入力してください。";
    cin >> x;
    return x;
}