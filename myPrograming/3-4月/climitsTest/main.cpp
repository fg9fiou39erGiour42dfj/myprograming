#include <climits> // 整数型の各型で表現できる値のmax/minを取得できるライブラリ
#include <iostream>

using namespace std;

int main()
{
    // 符号なし型のminは0であるため，提供されていない
    cout << "UCHAR_MAX: " << UCHAR_MAX << endl;
    cout << "SCHAR_MIN: " << SCHAR_MIN << endl;
    cout << "SCHAR_MAX: " << SCHAR_MAX << endl;
    cout << "CHAR_MIN: " << CHAR_MIN << endl;
    cout << "CHAR_MAX: " << CHAR_MAX << endl;
    cout << "SHRT_MIN: " << SHRT_MIN << endl;
    cout << "SHRT_MAX: " << SHRT_MAX << endl;
    cout << "USHRT_MAX: " << USHRT_MAX << endl;
    cout << "INT_MIN: " << INT_MIN << endl;
    cout << "INT_MAX: " << INT_MAX << endl;
    cout << "UINT_MAX: " << UINT_MAX << endl;
    cout << "LONG_MIN: " << LONG_MIN << endl;
    cout << "LONG_MAX: " << LONG_MAX << endl;
    cout << "ULONG_MAX: " << ULONG_MAX << endl;
}