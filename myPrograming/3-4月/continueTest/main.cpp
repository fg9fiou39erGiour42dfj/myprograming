#include <iostream>

using namespace std;

int main()
{
    int n;
    while (true) {
        cout << "指定した回数だけ入力する数値を加算します。" << endl;
        cin >> n;
        if (n > 0) {
            break;
        } else {
            cout << "不正な値が入力されました。" << endl;
            n = -1;
        }
    }

    cout << n << "回，入力された数値を加算します。" << endl
         << "負の値が入力された場合は回数に含みません。" << endl;
    int sum = 0;
    for (int i = 0; i < n; i++) {
        int x;
        cout << "正の整数を入力してください。" << endl;
        cin >> x;
        if (x < 0) {
            cout << "負の整数が入力されました。" << endl;
            --i; // continueを用いてループさせる場合，入力された回数に含まれてしまうためデクリメントする
            continue;
        } else {
            sum += x;
        }
    }
    cout << "入力された値の合計は，" << sum << "です。" << endl;
}