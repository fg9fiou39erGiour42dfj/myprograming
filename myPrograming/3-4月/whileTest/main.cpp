#include<iostream>

using namespace std;

int main() {
    int i;
    cout << "0が入力されるまでループします" << endl;
    while(true) {
        cin >> i;
        if(i == 0) {
            break;
        } else {
            cout << "入力された数値は" << i << "なのでループします" << endl;
        }
    }
    cout << "0が入力されたので処理を終了します" << endl;
}