#include <iostream>

using namespace std;

int main()
{
    int x, y;

    cout << "xの値を入力してください。" << endl;
    cin >> x;
    cout << "yの値を入力してください。" << endl;
    cin >> y;

    cout << "x = " << x << ", y = " << y << endl;

    if (x > y) {
        cout << "xよりyが小さいのでソートします" << endl;
        // ブロック内でしか使用しない変数はブロック内で宣言する(スコープ)
        int tmp = x;
        x = y;
        y = tmp;
    }
    cout << "x = " << x << ", y = " << y << endl;
}