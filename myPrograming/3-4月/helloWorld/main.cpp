#include <iostream> // cin(入力)，cout(出力)が組み込まれている
#include <stdio.h>  // printfが組み込まれている

using namespace std;

int main() {
    char welcome[] = "Hello World";
    char *hoge = welcome;
    printf("%s\n", welcome);
    printf("%s\n", hoge);
    return 0;
    // char welcome[] = "";
    // cin >> welcome;
    // cout << welcome << endl;
}