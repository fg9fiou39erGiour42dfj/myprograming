#include <iostream>

using namespace std;

// 複合代入演算子
int main()
{
    int x = 100, y = 10;
    x += y; // x = x + y
    cout << "x += y → x = " << x << endl;
    x -= y; // x = x - y
    cout << "x -= y → x = " << x << endl;
    x *= y; // x = x * y
    cout << "x *= y → x = " << x << endl;
    x /= y; // x = x / y
    cout << "x /= y → x = " << x << endl;
    x %= y; // x = x % y
    cout << "x %= y → x = " << x << endl;
}