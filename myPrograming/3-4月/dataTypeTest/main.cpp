#include <iostream>

using namespace std;

int main()
{
    // c++として保証されている値の範囲
    // 処理系に依存するが，符号付きは更に-1の範囲まで設定可能
    signed int a; // 符号付き整数型 -32767 ~ 32767
    unsigned int b; // 符号なし整数型 0 ~ 65535
    int c; // int = unsigned int
    char d; // 0 ~ 255 or -127 ~ 127 どちらになるかは処理系依存
    signed char e; // -127 ~ 127
    unsigned char f; // 0 ~ 255
    short int g; // -32767 ~ 32767
    unsigned short int h; // 0 ~ 65535
    long int i; // -2147483647 ~ 2147483647
    unsigned long int j; // 0 ~ 4294967295
}