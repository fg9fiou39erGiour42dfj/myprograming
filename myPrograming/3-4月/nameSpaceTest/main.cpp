#include <iostream>
#include <string.h>

using namespace std;

int x = 0;

namespace hoge {
int x = 1;
} // namespace hoge

namespace foo {
int x = 2;
} // namespace foo

/**
 * 名前空間にアクセスする場合は 名前空間名::変数/関数を指定する
 * 名前空間ごとにアクセスするため，同一変数名であっても衝突することはない
 * 本来はcoutやcin, endlも必要だが，stdをusingしているため省略している(復習)
 */
int main()
{
    cout << "名前空間なし: x = " << x << endl;
    cout << "名前空間hoge: hoge::x = " << hoge::x << endl;
    cout << "名前空間foo: foo:x = " << foo::x << endl;
}