#include <iostream>
#include <string.h>

using namespace std;

// ユーザークラス
class user {
private:
    string name;
    int age;
    int sex;
    string sexName;

public:
    user()
    {
        name = "";
        age = 0;
        sexName = setSexName(0);
    }
    user(string iName, int iAge, int iSex)
    {
        name = iName;
        age = iAge;
        sexName = setSexName(iSex);
    }
    string setSexName(int sex)
    {
        switch (sex) {
        case 1:
            sexName = "男";
            break;

        case 2:
            sexName = "女";
            break;
        default:
            sexName = "不明";
            break;
        };
        return sexName;
    }

    void outputInfo()
    {
        cout << "名前: " << name << endl;
        cout << "年齢: " << age << endl;
        cout << "性別: " << sexName << endl;
    }
};

// 入力クラス
class inputInfo {
public:
    string name;
    int age;
    int sex;
    inputInfo()
    {
        cout << "名前を入力してください。" << endl;
        cin >> name;
        cout << "年齢を入力してください。" << endl;
        cin >> age;
        cout << "男性の場合は1，女性の場合は2を入力してください。" << endl
             << "それ以外を入力すると不明扱いになります。" << endl;
        cin >> sex;
    }
};

int main()
{
    // ユーザー情報なしでユーザークラスをnewする
    // user noUser;
    // noUser.outputInfo();

    // 入力クラスで情報位を設定してからユーザークラスをnewする
    inputInfo userInput;
    user userInfo(userInput.name, userInput.age, userInput.sex);
    userInfo.outputInfo();
}