#include <iomanip> // setw(n)に必要なライブラリ
#include <iostream>

using namespace std;

int main()
{
    // 多重ループ
    for (int i = 1; i < 10; i++) {
        for (int j = 1; j < 10; j++) {
            // setw(n): 少なくともn桁の幅を空ける
            cout << setw(3) << i * j;
        }
        cout << endl;
    }
}