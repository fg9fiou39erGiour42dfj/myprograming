#include <iostream>
#include <string.h>

using namespace std;

int x = 0;

namespace hoge {
int x = 1;
} // namespace hoge

using namespace hoge;

int main()
{
    // 名前空間外で宣言したxと名前空間hogeのxが競合してコンパイルエラーとなる
    // むやみに名前空間の宣言はしないほうがリスクも下がる
    // cout << x << endl;
}
