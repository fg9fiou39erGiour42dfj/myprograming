#include <iostream>

using namespace std;

int main() {
    // 1つ目の要素数は省略可能
    int array[][4] = {
        {1, 2, 3, 4}
        , {5, 6, 7, 8}
        , {9, 10, 11, 12}
    };
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            cout << array[i][j] << endl;
        }
    }
}