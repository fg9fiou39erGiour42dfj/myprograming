#include<iostream>

using namespace std;

int main() {
    int i;
    cout << "数値を入力してください\n";
    cin >> i;

    switch(i) {
        case 0:
            cout << "入力された数値は0です" << endl;
        break;

        case 1:
        case 2:
        case 3:
            cout << "入力された数値は1〜3のどれかです" << endl;
        break;

        default:
            cout << "入力された数値は4以上です" << endl;
        break;
    }
}