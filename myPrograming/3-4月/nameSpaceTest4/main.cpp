#include <iostream>
#include <string.h>

using namespace std;

namespace hoge {
void output()
{
    cout << "ここはhoge" << endl;
}
} // namespace hoge

using namespace hoge;

namespace foo {
void output()
{
    cout << "ここはfoo" << endl;
}
void showOutput()
{
    // fooの名前空間内なのでここではfoo::output()が呼び出される
    output();
}
} // namespace foo

int main()
{
    output(); // hoge::output()が呼び出される
    foo::output(); // fooのoutput()が呼び出される
    foo::showOutput(); // foo::output()
}