#include <cstdlib>
#include <ctime>
//↑乱数生成に必要なヘッダファイル
// #include <random> //C++11以降で提供されているより高性能なヘッダファイル
#include <iostream>

using namespace std;

int main()
{
    int x;
    srand(time(NULL)); // これを実行することで乱数を設定する
    x = rand() % 10; // 0~9までの乱数。擬似乱数
    // x = rand() % 100; // 0~99までの乱数
    cout << "今回の乱数の値は，" << x << "です。" << endl;
}
