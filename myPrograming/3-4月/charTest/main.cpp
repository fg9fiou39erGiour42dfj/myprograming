#include <iostream>
#include <string>

using namespace std;

int main()
{
    char x;
    string y;

    x = 'a'; // charは文字リテラルのため，''(シングルクォート)で括る
    // x = ''; // × charは空文字を許容しない
    y = "hoge"; // stringは文字列リテラルのため，""(ダブルクォート)で括る
}