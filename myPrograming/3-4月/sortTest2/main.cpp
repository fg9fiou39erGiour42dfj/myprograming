#include <iostream>

using namespace std;

int main()
{
    int x, y, z;
    cout << "入力された3つの数字(x, y, z)を昇順にソートします。" << endl;
    cout << "xの値を入力してください。" << endl;
    cin >> x;
    cout << "yの値を入力してください。" << endl;
    cin >> y;
    cout << "zの値を入力してください。" << endl;
    cin >> z;

    cout << "ソート前…" << endl
         << "x = " << x << ", y = " << y << ", z = " << z << endl;

    if (x == y && y == z) {
        cout << "入力された3の数値は同値です。" << endl;
    } else {
        int tmp;
        if (x > y) {
            tmp = x;
            x = y;
            y = tmp;
        }
        if (x > z) {
            tmp = x;
            x = z;
            z = tmp;
        }
        if (y > z) {
            tmp = y;
            y = z;
            z = tmp;
        }
        cout << "ソート後…" << endl
             << "x = " << x << ", y = " << y << ", z = " << z << endl;
    }
}