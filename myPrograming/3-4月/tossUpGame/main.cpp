#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string.h>

using namespace std;

const int PLAY_COUNT = 5;

int playerToss();
int cpuToss();
string tossName(int toss);

/**
 * じゃんけんゲーム
 */
int main()
{
    int uToss, cToss;
    int winCnt = 0, loseCnt = 0;

    cout << "今からじゃんけんで" << PLAY_COUNT << "回勝負します。" << endl
         << "勝ち/負け回数を最後に表示します。" << endl
         << "あいこは回数に含みません。" << endl;

    int i = 1;
    while (i <= PLAY_COUNT) {
        string uTossName, cTossName;
        cout << i << "回目" << endl;

        uToss = playerToss();
        cToss = cpuToss();
        uTossName = tossName(uToss);
        cTossName = tossName(cToss);

        cout << "あなた: " << uTossName << endl
             << "CPU: " << cTossName << endl;
        if (uToss == cToss) {
            cout << "あいこです！" << endl;
        } else {
            if ((uToss == 1 && cToss == 2)
                || (uToss == 2 && cToss == 3)
                || (uToss == 3 && cToss == 1)) {
                cout << "あなたの勝ちです！" << endl;
                ++winCnt;
            } else {
                cout << "あなたの負けです…" << endl;
                ++loseCnt;
            }
            i++;
        }
    }
    cout << "今回の成績" << endl
         << "勝ち: " << winCnt << endl
         << "負け: " << loseCnt << endl
         << endl
         << "お疲れさまでした！" << endl;
}

/**
 * playerToss
 * プレイヤーのじゃんけんでだす値を決める
 * return int
 */
int playerToss()
{
    int uToss;
    while (true) {
        cout << "グーなら1，チョキなら2，パーなら3を入力してください。";
        cin >> uToss;
        if (uToss > 0 && uToss < 4) {
            break;
        } else {
            cout << "不正な値が入力されました。" << endl;
            uToss = 0;
        }
    }
    return uToss;
}

/**
 * cpuToss
 * CPUのじゃんけんでだす値を決める
 * return int
 */
int cpuToss()
{
    int cToss;
    srand(time(NULL)); // これを実行することで乱数を設定する
    cToss = (rand() % 2) + 1; // 1~3までの乱数。擬似乱数
    return cToss;
};

/**
 * tossName
 * じゃんけんで決まった数値によって名称文字列として変換する
 * return string
 */
string tossName(int toss)
{
    string tossName = "";
    switch (toss) {
    case 1:
        tossName = "グー";
        break;
    case 2:
        tossName = "チョキ";
        break;
    case 3:
        tossName = "パー";
        break;
    }
    return tossName;
}
