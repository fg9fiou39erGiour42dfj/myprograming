#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

int setNumber();
int setHighAndLow();
const int LIMIT_NUMBER = 100;

int main()
{
    cout << "ハイアンドローゲーム" << endl;
    ;
    int nowNumber;
    string retry;

    nowNumber = setNumber();
    do {
        cout << "現在の数値は，" << nowNumber << "です。" << endl
             << "値は1〜" << LIMIT_NUMBER << "の間です。" << endl;
        int highAndLow, comparisonNumber;
        highAndLow = setHighAndLow();
        comparisonNumber = setNumber();
        if (nowNumber == comparisonNumber) {
            cout << "同じ数字でしたので，やり直します。" << endl;
            retry = "Y";
        } else {
            if (
                (highAndLow && nowNumber < comparisonNumber)
                || (!highAndLow && nowNumber > comparisonNumber)) {
                cout << "正解です！" << endl
                     << "現在の数値: " << nowNumber << endl
                     << "比較用の数値: " << comparisonNumber << endl;
            } else {
                cout << "はずれです…" << endl
                     << "現在の数値: " << nowNumber << endl
                     << "比較用の数値: " << comparisonNumber << endl;
            }
            while (true) {
                cout << "続ける場合はY(y)，終了する場合はN(n)を入力してください。" << endl;
                cin >> retry;
                if (retry == "Y" || retry == "y" || retry == "N" || retry == "n") {
                    break;
                } else {
                    cout << "不正な値が入力されました。入力し直してください。" << endl;
                    retry = "";
                }
            }
            nowNumber = comparisonNumber;
        }
    } while (retry == "Y" || retry == "y");
    cout << "お疲れさまでした！" << endl;
}

int setNumber()
{
    int nowNumber;
    srand(time(NULL));
    nowNumber = (rand() % LIMIT_NUMBER) + 1;
    return nowNumber;
}

int setHighAndLow()
{
    int highAndLowFlg = -1;
    while (true) {
        cout << "今の数字より大きいと思う場合は1，小さいと思う場合は0を入力してください" << endl;
        cin >> highAndLowFlg;
        if (highAndLowFlg == 0 || highAndLowFlg == 1) {
            break;
        } else {
            cout << "不正な値が入力されました。入力し直してください。" << endl;
            highAndLowFlg = -1;
        }
    }
    return highAndLowFlg;
}