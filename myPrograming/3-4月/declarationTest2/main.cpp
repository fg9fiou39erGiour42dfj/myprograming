#include <cmath> // fmod(浮動小数点の割り算)に対応したライブラリ
#include <iostream>

using namespace std;

int main()
{
    double x, y;

    cout << "任意の数値(小数点あり)xを入力してください" << endl;
    cin >> x;
    cout << "任意の数値(小数点あり)yを入力してください" << endl;
    cin >> y;

    if (double mod = fmod(x, y)) {
        cout << "xはyで割り切れません。" << endl
             << "x = " << x << ", y = " << y << ", 余り: " << mod << endl;
    } else {
        int quotient = (x / y);
        cout << "xはyで割り切れました。" << endl
             << "x = " << x << ", y = " << y << ", 商: " << quotient << endl;
    }
}