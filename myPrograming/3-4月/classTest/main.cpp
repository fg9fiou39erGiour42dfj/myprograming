#include <iostream>
#include <string.h>

using namespace std;

class human {
private:
    string name;
    int age;

public:
    human(string iName, int iAge)
    {
        name = iName;
        age = iAge;
    }
    void showInputData()
    {
        cout << "名前: " << name << endl;
        cout << "年齢: " << age << endl;
    }
};

int main()
{
    string name;
    int age;
    cout << "名前を入力してください。" << endl;
    cin >> name;
    cout << "年齢を入力してください。" << endl;
    cin >> age;
    human inputData(name, age);
    inputData.showInputData();
}