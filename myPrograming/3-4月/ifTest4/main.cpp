#include <iostream>

using namespace std;

int main()
{
    int x, y, max, min;
    cin >> x;
    cin >> y;
    // {}で括るとブロック(複合文)になる
    if (x < y) {
        max = y;
        min = x;
    } else {
        max = x;
        min = y;
    }
    // ブロックにしない場合，コンマ(カンマ？)演算子でも同じことが可能。結果は上記と一緒
    // if (x < y)
    //     max = y, min = x;
    // else
    //     max = x, min = y;
    cout << "max: " << max << ", min = " << min << endl;
}