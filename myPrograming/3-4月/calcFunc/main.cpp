#include <iostream> // cin(入力)，cout(出力)が組み込まれている
#include <stdio.h>  // printfが組み込まれている

using namespace std;

// 関数は先に宣言しておく
// 先に宣言だけして後ろに書くのもOK
// int addCalc(int x, int y) {
//     int z = x + y;
//     return z;
// }
int addCalc(int x, int y);
int subCalc(int x, int y);
int mulCalc(int x, int y);
int divCalc(int x, int y);
int remCalc(int x, int y);

int main() {
    int x, y;
    cout << "xの値を入力してください。";
    cin >> x;
    cout << "yの値を入力してください。";
    cin >> y;
    int z;
    z = addCalc(x, y);
    cout << "x + y = " << z << endl;
    z = subCalc(x, y);
    cout << "x - y = " << z << endl;
    z = mulCalc(x, y);
    cout << "x * y = " << z << endl;
    z = divCalc(x, y);
    cout << "x / y = " << z << endl;
    z = remCalc(x, y);
    cout << "x % y = " << z << endl;
}

int addCalc(int x, int y) {
    int z = x + y;
    return z;
};

int subCalc(int x, int y) {
    int z = x - y;
    return z;
};

int mulCalc(int x, int y) {
    int z = x * y;
    return z;
};

int divCalc(int x, int y) {
    int z = x / y;
    return z;
};

int remCalc(int x, int y) {
    int z = x % y;
    return z;
};
