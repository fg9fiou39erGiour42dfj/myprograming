#include <iostream>
#include <string.h>

using namespace std;

class human {
protected:
    string name;
    int age;

public:
    human(string iName, int iAge)
    {
        name = iName;
        age = iAge;
    }
};

/**
 * 継承する場合は class クラス名: 修飾子 継承元クラス名
 * 修飾子はpublic, protected, privateの3種
 * public: 継承元で設定した修飾子をそのまま引き継ぐ
 * protected: 継承元でpublicだったものがprotectedに，それ以外はそのまま
 * private: 継承元の修飾子に関わらずすべてprivateで引き継ぐ
 */

class japan : public human {
private:
    string national;

public:
    // コンストラクタで継承元を使用する場合
    japan(string iName, int iAge)
        : human(iName, iAge)
    {
        national = "日本";
    }
    void showInputData()
    {
        cout << "名前: " << name << endl;
        cout << "年齢: " << age << endl;
        cout << "国: " << national << endl;
    }
};

class global : public human {
private:
    string national;

public:
    global(string iName, int iAge)
        : human(iName, iAge)
    {
        national = "日本以外";
    }
    void showInputData()
    {
        cout << "名前: " << name << endl;
        cout << "年齢: " << age << endl;
        cout << "国: " << national << endl;
    }
};

int main()
{
    string name;
    int age;
    int national;
    cout << "名前を入力してください。" << endl;
    cin >> name;
    cout << "年齢を入力してください。" << endl;
    cin >> age;
    while (true) {
        cout << "日本人なら1，違うなら0を入力してください。";
        cin >> national;
        if (national == 1) {
            japan inputData(name, age);
            inputData.showInputData();
            break;
        } else if (national == 0) {
            global inputData(name, age);
            inputData.showInputData();
            break;
        } else {
            cout << "0, 1以外が入力されました" << endl;
        }
    }
}