#include <iostream>
#include <string.h>

using namespace std;

namespace hoge {
void output()
{
    cout << "名前空間名: hoge" << endl;
}
void outputFoo()
{
    // まだfooの名前空間を宣言していないためアクセスできない
    // foo::output();
}

} // namespace hoge

namespace foo {
void output()
{
    cout << "名前空間名: foo" << endl;
}
void outputHoge()
{
    // 別な名前空間に対してもアクセスできる
    hoge::output();
}
} // namespace foo

int main()
{
    hoge::output(); //hoge
    foo::output(); //foo
    foo::outputHoge(); //hoge
}