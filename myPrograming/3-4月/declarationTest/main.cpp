#include <iostream>

using namespace std;

int main()
{
    int x;
    cout << "xの値を入力してください。" << endl;
    cin >> x;
    if (x > 0) {
        // 条件分岐内で変数を宣言することも可能
        // 宣言された変数はその条件分岐内では使用可能
        if (int mod = x % 2) {
            cout << "入力された値は奇数です" << endl;
        } else {
            cout << "入力された値は偶数です" << endl;
        }
    } else {
        cout << "負の値が入力されました" << endl;
    }
}