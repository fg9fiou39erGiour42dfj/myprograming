#include <iostream>

using namespace std;

int main()
{
    int x, y;
    cout << "2つの数字を続けて入力してください。" << endl;
    cin >> x;
    cin >> y;
    // 3項演算子
    x == y
        ? cout << "xとyは同値です。"
        : x < y
            ? cout << "1番目の数値より2番目の数値のほうが大きいです。x = " << x << ", y = " << y << endl
            : cout << "1番目の数値より2番目の数値のほうが小さいです。x = " << x << ", y = " << y << endl;
}