#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "直角二等辺三角形を中抜きで表示します。" << endl;
    while (true) {
        cout << "段数を指定してください。(2段以上)" << endl;
        cin >> n;
        if (n > 2) {
            break;
        } else {
            cout << "不正な値が入力されました。" << endl;
            n = 0;
        }
    }
    cout << n << "段で表示します。" << endl
         << endl;

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= i; j++) {
            //最終段以外は最初と最後のみ，最終段はすべて表示する
            if (j == 1 || j == i || i == n) {
                cout << "*";
            } else {
                cout << " ";
            }
        }
        cout << endl;
    }
}