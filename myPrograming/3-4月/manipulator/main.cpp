#include <iomanip> // setw(n)を使用するのに必要
#include <iostream>

using namespace std;

int main()
{

    cout << oct << 1234 << endl; // 8進数
    cout << dec << 1234 << endl; // 10進数
    cout << hex << 1234 << endl; // 16進数

    cout << showbase; // 出力の前に基数表示を付加する

    cout << oct << 1234 << endl; // 8進数
    cout << dec << 1234 << endl; // 10進数
    cout << hex << 1234 << endl; // 16進数

    cout << noshowbase; // 出力の前に基数表示を付加しない

    cout << setw(5) << internal << "abc" << endl; // 文字埋めを中央にいれて表示する
    cout << setw(5) << left << "abc" << endl; // 左寄せ(文字埋めは右側)
    cout << setw(5) << right << "abc" << endl; // 右寄せ(文字埋めは左側)

    cout << setbase(10); // setbase(n): n進数で入出力する

    cout << setw(5) << internal << -123 << endl;
    cout << setw(5) << left << -123 << endl;
    cout << setw(5) << right << -123 << endl;

    cout << setfill('*'); // setfill(文字): カッコ内で指定した文字埋めする。文字リテラル

    cout << setw(10) << internal << -123 << endl;
    cout << setw(10) << left << -123 << endl;
    cout << setw(10) << right << -123 << endl;

    cout << setfill(' '); // 文字埋めを半角スペースにする

    cout << fixed << setw(10) << setprecision(4) << 123.4 << endl;
    // fixed: 固定浮動小数点記法にする
    // setprecision(n): 表示をn桁表示にする
    cout << scientific << setw(10) << setprecision(4) << 123.4 << endl;
    // scientific: 指数付き記法にする。指数部は最低2桁
}