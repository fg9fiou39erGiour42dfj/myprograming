#include "main.h"  // ヘッダファイル。""で括るのは独自定義のファイルだから

#include <iostream>  // 定義済みのファイル。こちらは<>で括る

using namespace std;

int main() {
    int x, y;
    cout << "xの値を入力してください。\n";
    cin >> x;
    cout << "yの値を入力してください。\n";
    cin >> y;
    int z = addCalc(x, y);
    cout << "x + y = " << z << endl;
}