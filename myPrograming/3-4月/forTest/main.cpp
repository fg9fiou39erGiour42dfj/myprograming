#include <iostream>

using namespace std;

void forEcho(int x);

int main() {
    int x;
    cout << "xの数字を入力してください。入力された数字までループして表示します"
            "\n";
    cin >> x;
    forEcho(x);
    return 0;
}

void forEcho(int x) {
    for(int y = 1; y <= x; y++) {
        cout << y << endl;
    }
}
