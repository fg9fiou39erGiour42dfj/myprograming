#include <cstddef>
#include <iostream>
#include <typeinfo>

using namespace std;

int main()
{
    char c;
    cout << "char型の大きさ: " << sizeof(char) << endl;
    cout << "変数cの大きさ: " << sizeof(c) << endl;
    // char型は必ず1

    // 以下は処理系に依存
    short h;
    cout << "short型の大きさ: " << sizeof(short) << endl;
    cout << "変数hの大きさ: " << sizeof(h) << endl;
    // 結果: 2

    int i;
    cout << "int型の大きさ: " << sizeof(int) << endl;
    cout << "変数iの大きさ: " << sizeof(i) << endl;
    // 結果: 4

    long l;
    cout << "long型の大きさ: " << sizeof(long) << endl;
    cout << "変数lの大きさ: " << sizeof(l) << endl;
    // 結果: 8

    // sizeof(short) <= sizeof(int) <= sizeof(long)
    // これは処理系によらず必ず成立する

    typedef short hoge; // typedefされた型名は必ず符号なしとなる
    hoge foo; // これはunsigned shot型の変数fooとなる

    cout << "char: " << typeid(char).name() << endl; // 結果: c
    cout << "short: " << typeid(short).name() << endl; // 結果: s
    cout << "int: " << typeid(int).name() << endl; // 結果: i
    cout << "long: " << typeid(long).name() << endl; // 結果: l
    // typeidの表記が1文字で省されている？
}