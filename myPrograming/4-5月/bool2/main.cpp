#include <iostream>

using namespace std;

int main()
{
    int a, b;
    cout << "aに設定する数値を入力してください" << endl;
    cin >> a;
    cout << "bに設定する数値を入力してください" << endl;
    cin >> b;

    cout << endl
         << "a: " << a << ", b: " << b << endl;
    cout << boolalpha;

    cout << "a < b: " << (a < b) << endl;
    cout << "a <= b: " << (a <= b) << endl;
    cout << "a > b: " << (a > b) << endl;
    cout << "a >= b: " << (a >= b) << endl;
    cout << "a == b: " << (a == b) << endl;
    cout << "a != b: " << (a != b) << endl;
    cout << "!a: " << (!a) << endl;
    cout << "!b: " << (!b) << endl;

    cout << noboolalpha;
}