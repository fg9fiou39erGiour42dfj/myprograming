#include <iostream>

using namespace std;

int main()
{
    // boolalphaを使わないとtrue: 1，false: 0で出力される
    cout << true << endl; // 1
    cout << false << endl; // 0

    // boolalphaを用いるとtrue, falseで出力される
    cout << boolalpha;
    cout << true << endl; // true
    cout << false << endl; // false

    // 再度0, 1で表示する場合はnoboolalphaを用いる
    cout << noboolalpha;
    cout << true << endl; // 1
    cout << false << endl; // 0
}