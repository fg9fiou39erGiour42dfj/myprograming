#include <climits>
#include <iostream>

using namespace std;

int main()
{
    cout << "処理系の確認" << endl
         << (CHAR_MIN ? "符号付き" : "符号なし")
         << "文字型" << endl;
    // 結果: 符号付き
    /**
     * charが符号付きの場合
     * #define CHAR_MIN = SCHAR_MIN // 最小値は signed charと一緒
     * #define CHAR_MAX = SCHAR_MAX // 最大値は signed charと一緒
     * 
     * charが符号なしの場合
     * #define CHAR_MIN = 0         // 最小値は0
     * #define CHAR_MAX = UCHAR_MAX // 最大値は unsigned charと一緒
     */

    cout << "処理系の文字列ビット数確認" << endl
         << CHAR_BIT << "ビット" << endl;
    // 結果: 8ビット
    // 9ビットや32ビットの処理系も存在する
}