#include <iostream>
#include <limits>

using namespace std;

int main()
{
    // 以前行ったclimitsライブラリと同じようなもの
    // climits: c言語用，limits: c++用
    cout << "この処理系の整数型で表現できる値" << endl;
    cout << "char: "
         << int(numeric_limits<char>::min()) << "〜"
         << int(numeric_limits<char>::max()) << endl;
    cout << "int: "
         << int(numeric_limits<int>::min()) << "〜"
         << int(numeric_limits<int>::max()) << endl;
    // 以下同じような記載方法のため略

    // 符号付き判定
    cout << (numeric_limits<char>::is_signed ? "符号付き" : "符号なし")
         << "文字列" << endl;
}