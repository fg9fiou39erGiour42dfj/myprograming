#include <iostream>
#include <limits>

using namespace std;

int main()
{
    cout << "最小値: " << numeric_limits<double>::min() << endl;
    cout << "最大値: " << numeric_limits<double>::max() << endl;
    cout << "仮数部: " << numeric_limits<double>::radix << "進数で"
         << numeric_limits<double>::digits << "桁" << endl;
    cout << "桁数: " << numeric_limits<double>::digits10 << endl;
    cout << "機械ε: " << numeric_limits<double>::round_error() << endl;
    cout << "丸め方式: ";

    switch (numeric_limits<double>::round_style) {
    case round_indeterminate:
        cout << "決定できない" << endl;
        break;
    case round_toward_zero:
        cout << "ゼロに向かって丸める" << endl;
        break;
    case round_to_nearest:
        cout << "表現可能な最も近い値に丸める" << endl;
        break;
    case round_toward_infinity:
        cout << "無限大に向かって丸める" << endl;
        break;
    case round_toward_neg_infinity:
        cout << "負の無限大に向かって丸める" << endl;
    }
}