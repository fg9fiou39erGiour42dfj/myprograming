#include <iomanip>
#include <iostream>

using namespace std;

int main()
{
    float a = 123456789.0;
    double b = 12345678901234567890.0;
    long double c = 123456789012345678901234567890.0;

    // setprecision(n): 浮動小数点数を出力する制度をn桁に設定する
    cout << "a = " << setprecision(30) << a << endl; // 123456792
    cout << "b = " << setprecision(30) << b << endl; // 12345678901234567168
    cout << "c = " << setprecision(30) << c << endl; // 123456789012345677877719597056
    // 浮動小数点型の表現範囲は"大きさ"と"精度"の両方の制限を受けるため，正確に表現できない場合がある

    cout << "12.3: " << 12.3 << endl; // double型
    cout << "12.3F: " << 12.3F << endl; // float型
    cout << "12.3L: " << 12.3L << endl; // long float型
}